import sys, os, struct
import bx.bbi.bigwig_file
from sklearn.metrics import mutual_info_score
from scipy.ndimage import gaussian_filter1d
import scipy.stats as SS
import numpy as np

def get_chrom_sizes(bwname):
  csize = {}
  fh = open(os.path.expanduser(bwname), "rb")
  magic = fh.read(4)
  if magic == '&\xfc\x8f\x88':
    endianness = '<'
  elif magic == '\x88\x8f\xfc&':
    endianness = '>'
  else:
    raise IOError("The file is not in bigwig format")
  (version,zoomLevels,chromosomeTreeOffset,fullDataOffset,fullIndexOffset,fieldCount,definedFieldCount,autoSqlOffset,totalSummaryOffset,uncompressBufSize,reserved)=struct.unpack(endianness+'HHQQQHHQQIQ',fh.read(60))
  if version < 3:
    raise IOError("Bigwig files version <3 are not supported")
  fh.seek(chromosomeTreeOffset)
  magic = fh.read(4)
  if magic == '\x91\x8c\xcax':
    endianness = '<'
  elif magic == 'x\xca\x8c\x91':
    endianness = '>'
  else:
    raise ValueError("Wrong magic for this bigwig data file")
  (blockSize, keySize, valSize, itemCount, reserved) = struct.unpack(endianness + 'IIIQQ', fh.read(28))
  (isLeaf, reserved, count) = struct.unpack(endianness + 'BBH', fh.read(4))
  for n in range(count):
    (key, chromId, chromSize) = struct.unpack(endianness + str(keySize) + 'sII', fh.read(keySize + 2 * 4))
    csize[key.replace('\x00', '')] = chromSize
  return csize


def couple_idx(indices, signal):
  if len(indices) == 0:
    return np.array([0, len(signal)])
  first_left = True
  odd_boundary = False
  if np.min(signal[indices[0]:indices[1]]) < 0 and np.max(signal[indices[0]:indices[1]]) < 0:
    # the first index is a left boundary of a peak as all signal is < 0 up to it
    first_left = False
  n_boundary = len(indices)
  if n_boundary // 2 * 2 != n_boundary:
    odd_boundary = True
  if odd_boundary:
    if first_left:
      indices = np.append(indices, len(signal))
    else:
      indices = np.append(0, indices)
  indices = indices.reshape((len(indices) / 2, 2))      
  indices[:, 1] += 1
  return indices[np.ravel(np.diff(indices, axis=1) > 0)]
    
def zpdl(x, w):
#  x = len(s)
  o = x // 2 // w
  if o * 2 * w == x:
    return 0
  n = o // w
  nx = (n + o) * 2 * w
  retval = nx - x
  if retval < 0:
    retval = 2 * w + retval
  return retval

def _prepare_signal(signal, window_size):
  padded_signal = np.zeros(zpdl(len(signal), window_size) + len(signal))
  padded_signal[-len(signal):] = signal
  padded_signal = padded_signal.reshape((len(padded_signal) // window_size // 2, 2, window_size))
  return padded_signal

def find_peak(signal, window_size):
  peak_list = []
  padded_signal = _prepare_signal(signal, window_size)
  mutual_info_array = np.array([mutual_info_score(*padded_signal[x]) for x in range(len(padded_signal))])
  mi_distr = SS.gamma(*SS.gamma.fit(mutual_info_array))
  t_mutual_info_array = mutual_info_array - np.median(mutual_info_array)
  t_mutual_info_array = gaussian_filter1d(t_mutual_info_array, 1)
  zero_crossing = np.where(np.diff(np.sign(t_mutual_info_array)))[0]
  if len(zero_crossing) < 2:
    return np.array([])
  for x in couple_idx(zero_crossing, t_mutual_info_array):
    (s, e) = x
    (ss, ee) = x * window_size * 2
    pvalue = 1 - mi_distr.cdf(np.max(mutual_info_array[s:e]))
    peak_list.append([ss, ee, pvalue])
  return np.array(peak_list  )
  

def refine_peak(peak_idx, signal, window_size, step = None, threshold = 0.05):
  if not step:
    step = 10**(int(np.floor(np.log10(window_size)) - 1))
  if step < 10:
    step = 10
      
  (left, right) = peak_idx
  left = np.max([np.min([left, left - window_size // 2]), 0])
  right = np.min([np.max([right, right + window_size // 2]), len(signal)])
  local_signal = _prepare_signal(signal[left:right], step)
  mutual_info_array = np.array([mutual_info_score(*local_signal[x]) for x in range(len(local_signal))])
  mi_distr = SS.gamma(*SS.gamma.fit(mutual_info_array))
  try:
    (left_o, right_o) = np.where(mi_distr.cdf(mutual_info_array) > 1 - threshold)[0][[0, -1]] * step
  except IndexError:
    return np.array([np.nan, np.nan])
  local_signal = local_signal.ravel()
  local_length = len(local_signal)
  if right_o != local_length:
    right_o = np.argmin(local_signal[right_o:]) + right_o
    if right_o > local_length:
      right_o =  local_length
  if left_o > 0:    
    left_o = left_o - np.argmin(local_signal[:left_o][::-1])
    if left_o < 0:
      left_o = 0
  return np.array([left + left_o, left + right_o]  )
  

  


ws = [10000, 20000, 50000]
#ws = [1000, 2000, 5000, 10000, 20000, 50000, 100000]

fname = sys.argv[1]
bwh = bx.bbi.bigwig_file.BigWigFile(open(fname, 'rb'))
chrom_sizes = get_chrom_sizes(fname)

for chrom in chrom_sizes.keys():
  sys.stderr.write("%s\n" % chrom)
  data = bwh.get_as_array(chrom, 0, chrom_sizes[chrom])
  data[np.isnan(data)] = 0
  for win_size in ws:
    sys.stderr.write("Finding peaks")
    peaks = find_peak(data, win_size)
    rpeaks = []
    sys.stderr.write("  Refining peaks\n")
    for peak in peaks:
      start, end = refine_peak(peak[:2], data, win_size)
      if np.isfinite(start):
        rpeaks.append([start, end, -10 * np.log10(peak[2])])
    if len(rpeaks) == 0:
      continue
    sys.stderr.write("Writing file\n")
    fout = open("%s.%s.%d.bed" % ('.'.join(os.path.basename(fname).split('.')[:-1]), chrom, win_size), 'w')
    for p in rpeaks:
      fout.write("%s\t%d\t%d\t%.4e\n" % (chrom, p[0], p[1],p[2]))
    fout.close()  
#
#
#m = []
#raw_peaks = []
#pvalues = []
#for w in ws:
#  raw_peaks.append([])
#  pvalues.append([])
#  for x in couple_idx(zero_crossing, mut_):
#    (s, e) = x
#    (ss, ee) = x * w * 2
#    raw_peaks[-1].append([ss, ee])
#    pvalues[-1].append(1 - g_dist.cdf(np.max(mut_[s:e])))
#  m.append(mut_)
#  print(w)
#
#
#im = np.zeros((len(ws), len(b)))
#for n in range(len(ws)):
#    zero_crossing = np.where(np.diff(np.sign(m[n])))[0]
#    for x in couple_idx(zero_crossing, m[n]):
#        (s, e) = x
#        (ss, ee) = x * ws[n] * 2
#        im[n][ss:ee]  = np.max(m[n][s:e]) / np.max(m[n])
#
#subplot(211)
#plot(b)
#subplot(212)
#imshow(im, aspect='auto')